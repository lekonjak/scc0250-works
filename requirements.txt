glfw==1.11.0
numpy==1.18.2
Pillow==7.1.2
PyGLM==1.2.0
PyOpenGL==3.1.5
simpleaudio==1.0.4
